package com.example.blog.control;


import com.example.blog.domain.Role;
import com.example.blog.domain.User;
import com.example.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    public String userList(
            Model model
    ) {
        model.addAttribute("users", userService.findAll());

        return "userList";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("{user}")
    public String userEditForm(
            @PathVariable User user,
            Model model
    ) {
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return "userEdit";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping
    public String userSave(
            @RequestParam Map<String, String> form,
            @RequestParam("userId") User user
    ) {
        userService.saveUser(user,  form);

        return "redirect:/user";
    }

    @GetMapping("profile")
    public String getProfile(
    ) {
        return "profile";
    }

    @PostMapping("profile")
    public String updateProfile(
            @RequestParam("password2") String passwordConfirm,
            @RequestParam String password,
            @AuthenticationPrincipal User user,
            Model model
    ) {
        boolean isConfirmEmpty = StringUtils.isEmpty(passwordConfirm) && StringUtils.isEmpty(password);
        boolean isPasswordDifferent = java.util.Objects.equals(password, passwordConfirm);


        if (isConfirmEmpty) {
            model.addAttribute("passwordError", "Passwords can't be empty");
            return "profile";
        } else {
            if (!isPasswordDifferent) {
                model.addAttribute("passwordError", "Passwords are different!");
                return "profile";
            }
        }
        userService.updateProfile(user, password);
        model.addAttribute("message", "Successfully changed");

        return "redirect:/user/profile";

    }
}
