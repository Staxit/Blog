<#include "security.ftl">
<#macro edit isEdit >
<!--Add message button -->
<a class="btn btn-primary" <#if isEdit>style="visibility: hidden" </#if>data-toggle="collapse" href="#collapseExample"
   role="button" aria-expanded="false" aria-controls="collapseExample">
    <#if isEdit>Edit<#else>Add message</#if>
</a>
<div class="collapse <#if message??>show</#if>" id="collapseExample">
    <div class="form-group mt-3">
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <input type="text" class="form-control ${(textError??)?string('is-invalid', '')}"
                       value="<#if message??>${message.text}</#if>" name="text" placeholder="Enter your message"/>
                <#if textError??>
                    <div class="invalid-feedback">
                        ${textError}
                    </div>
                </#if>
            </div>
            <div class="form-group">
                <input type="text" class="form-control ${(tagError??)?string('is-invalid', '')}"
                       value="<#if message??>${message.tag}</#if>" name="tag" placeholder="Tag"/>
                <#if tagError??>
                    <div class="invalid-feedback">
                        ${tagError}
                    </div>
                </#if>
            </div>
            <div class="form-group">
                <div class="custom-file">
                    <input type="file" name="file" id="customFile">
                    <label class="custom-file-label" for="customFile">Upload your picture</label>
                </div>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <#if user??>
            <input type="hidden" name="id" value="<#if message??>${message.id}</#if>"/>
            </#if>
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><#if isEdit>Save<#else>Add</#if></button>
            </div>
        </form>
    </div>
</div>
</#macro>
