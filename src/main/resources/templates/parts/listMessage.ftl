<#include "security.ftl">

<!--Message mapping -->
<div>
    <#list messages as message>
        <div class="card my-3">
        <#if message.filename??>
        <img src="/img/${message.filename}" class="card-img-top">
        </#if>
            <div class="card-header text-muted">
                <a href="/user-messages/${message.author.id}">${message.authorName}</a>
                <#if (message.author.id == currentUserId) || isAdmin>
                    <#include "deleteMessage.ftl">
                </#if>
                <#if message.author.id == currentUserId>
                <a class="btn btn-primary mr-2" style="float: right"
                   href="/user-messages/${message.author.id}?message=${message.id}">
                    Edit
                </a>
                </#if>
            </div>
            <div class="m-2">
                <span>${message.text}</span>
                <div align="right"><i> Tag:${message.tag}</i></div>
            </div>
        </div>
    <#else>
    <div class="mt-2">No message</div>
    </#list>
</div>

