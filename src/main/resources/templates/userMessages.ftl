<#import "parts/common.ftl" as c>
<#import "parts/editMessage.ftl" as e>

<@c.page>
    <#if isCurrentUser>
        <@e.edit true></@e.edit>
    </#if>
    <#include "parts/listMessage.ftl">
</@c.page>