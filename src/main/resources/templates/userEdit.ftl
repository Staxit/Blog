<#import "parts/common.ftl" as c>

<@c.page>
    <#if message??>
<div>
    ${message}
</div>
    </#if>
<form action="/user" method="post">
    <h4>User: ${user.username}</h4>
    <#list roles as role>
    <div>
        <label><input type="checkbox" name="${role}" ${user.roles?seq_contains(role)?string("checked", "")}>${role}
        </label>
    </div>
    </#list>
    <input type="hidden" value="${user.id}" name="userId">
    <input type="hidden" value="${_csrf.token}" name="_csrf">
    <button type="submit" class="btn btn-primary">Save</button>

</form>
</@c.page>